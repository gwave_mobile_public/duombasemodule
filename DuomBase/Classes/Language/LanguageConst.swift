//
//  LanguageModel.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/8/23.
//

import Foundation

/*
 imp_loading
 imp_loading_wait
 imp_recent
 imp_max_images
 imp_cancel
 imp_ok
 imp_cancel_payment
 imp_cancel_order
 imp_album_permission_tip
 imp_open
 imp_next
 imp_confirm
 imp_try_gifs
 imp_search
 imp_sticker_empty
 imp_download
 imp_crop
 imp_stickers
 imp_album
 imp_video
 imp_cam
 imp_gif
 imp_camera_no_permission
 imp_discard
 imp_photo_discard_tip
 imp_fail
 imp_camera_not_start
 imp_camera_fail_start
 imp_gif_hi
 imp_gif_haha
 imp_gif_love
 imp_gif_cat
 imp_gif_dog
 imp_gif_happy
 imp_gif_sad
 imp_video_path_empty
 imp_video_discard_tip
 imp_asset_nil
 imp_export_video_error
 imp_save
 */

/*
 loading···
 Loading, please wait.
 Recent
 Select a maximum of %d images
 Cancel
 OK
 Are you sure you want to cancel payment?
 Cancel Order
 We need permission to access all of your albums.
 Open
 Next
 Confirm
 Can‘t find the right photo? Try some GIFs!
 Search
 You don‘t have a sticker
 DOWNLOAD
 Crop
 Stickers
 ALBUM
 VIDEO
 CAM
 GIF
 lock fo camera permissions
 Discard
 Your photo has not been saved yet. Are you sure you want to discard?
 Failed
 Can‘t start camera
 Camera failed to start
 Hi
 haha
 love
 cat
 dog
 happy
 sad
 src is null
 Your video has not been saved yet. Are you sure you want to discard?
 Asset is nil
 export video error
 Save
 */


public let LanguageKey = "NativeLanguage"

public var LanguageMap: [String : String] = [
    "loading···": "imp_loading",
    "Loading, please wait.": "imp_loading_wait",
    "Recent": "imp_recent",
    "Select a maximum of %d images": "imp_max_images",  // 动态化的可以不加 走的另一个方法
    "Cancel": "imp_cancel",
    "OK": "imp_ok",
    "Are you sure you want to cancel payment?": "imp_cancel_payment",
    "Cancel Order": "imp_cancel_order",
    "We need permission to access all of your albums.": "imp_album_permission_part_title",
    "Open": "imp_open",
    "Next": "imp_next",
    "Confirm": "imp_confirm",
    "Can‘t find the right photo? Try some GIFs!": "imp_try_gifs",
    "Search": "imp_search",
    "You don‘t have a sticker": "imp_sticker_empty",
    "DOWNLOAD": "imp_download",
    "Crop": "imp_crop",
    "Stickers": "imp_stickers",
    "ALBUM": "imp_album",
    "VIDEO": "imp_video",
    "CAM": "imp_cam",
    "GIF": "imp_gif",
    "lock fo camera permissions": "imp_camera_no_permission",
    "Discard": "imp_discard",
    "Your photo has not been saved yet. Are you sure you want to discard?": "imp_photo_discard_tip",
    "Failed": "imp_fail",
    "Can‘t start camera": "imp_camera_not_start",
    "Camera failed to start": "imp_camera_fail_start",
    "Hi": "imp_gif_hi",
    "haha": "imp_gif_haha",
    "love": "imp_gif_love",
    "cat": "imp_gif_cat",
    "dog": "imp_gif_dog",
    "happy": "imp_gif_happy",
    "sad": "imp_gif_sad",
    "src is null": "imp_video_path_empty",
    "Your video has not been saved yet. Are you sure you want to discard?": "imp_video_discard_tip",
    "Asset is nil": "imp_asset_nil",
    "export video error": "imp_export_video_error",
    "Save": "imp_save",
    "Allow Stanly to access your photos and videos": "imp_auth_photo_title",
    "How you’ll use this": "imp_auth_you_use_title",
    "How we’ll use this": "imp_auth_we_use_title",
    "How these settings work": "imp_auth_settings_title",
    "Enable Library Access": "imp_auth_photo_button",
    "To add photos and videos from your camera roll to Stanly and apply effects to your photos and videos.": "imp_auth_photo_you_use_desc",
    "To let you share photos and videos from your camera roll to Stanly, apply effects and suggest photos and videos to share.": "imp_auth_photo_we_use_desc",
    "You can change your choice at any time in your device settings. If you allow access now, you won’t have to allow it again.": "imp_auth_photo_settings_desc",
    "This allows Stanly to share photos from your library and save photos to your camera roll.": "imp_auth_part_desc",
    "Allow Stanly to access your camera and microphone": "imp_auth_camera_title",
    "To take photos, record videos and preview visual and audio effects.": "imp_auth_camera_you_use_desc",
    "To show you previews of visual and audio effects.": "imp_auth_camera_we_use_desc",
    "Open Settings": "imp_auth_camera_button",
]
