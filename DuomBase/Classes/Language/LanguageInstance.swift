//
//  LanguageInstance.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/8/23.
//

import Foundation

public enum LanguageDynamicKey: String {
    case none
    case imp_max_images = "imp_max_images"
}

public class LanguageInstance {
    public static let shared = LanguageInstance()
    
    public var language: [String: String]?
    
    public init() {}
    
    @discardableResult
    public func getLanguageData() -> [String: String]? {
        if let json = UserDefaults.standard.value(forKey: LanguageKey) as? String,
           let data = json.data(using: .utf8, allowLossyConversion: false) {
            do {
                if let dic = try JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [String: String] {
                    language = dic
                    return dic
                }
            } catch {
                Logger.log(message: "Language Json解析出错", level: .error)
                return nil
            }
        }
        return nil
    }
}

public extension String {
    var localized: String {
        return local()
    }
    
    func local() -> String {
        guard !self.isEmpty else { return self }
        var language: [String: String]?
        if let lan = LanguageInstance.shared.language {
            language = lan
        } else {
            language = LanguageInstance.shared.getLanguageData()
        }
        
        if let language = language {
            return language[transToLanguageKey()] ?? self
        }
        return self
    }
    
    func transToLanguageKey() -> String {
        return LanguageMap[self] ?? self
    }
    
    func localDynamic(_ type: LanguageDynamicKey, dynamicCount: Int) -> String {
        guard !self.isEmpty else { return self }
        var language: [String: String]?
        var tempString: String = self
        if let lan = LanguageInstance.shared.language {
            language = lan
        } else {
            language = LanguageInstance.shared.getLanguageData()
        }

        if let language = language {
            tempString = language[type.rawValue] ?? self
        }
        return tempString.replaceStringWithPattern(count: dynamicCount)
    }
    
    func replaceStringWithPattern(count: Int) -> String {
        let pattern = "%d"
        let replacement = "\(count)"
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            let range = NSRange(location: 0, length: self.utf16.count)
            return regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replacement)
        } catch {
            Logger.log(message: "正则表达式出错: \(error)", level: .warning)
        }
        return self
    }
}

public extension Array {
    var localized: [String] {
        return localItem()
    }
    
    func localItem() -> [String] {
        let localArray = [String]()
        guard isNotEmpty else { return localArray }
        guard let ary = self as? [String] else { return localArray }
        return ary.map { $0.localized }
    }
}
