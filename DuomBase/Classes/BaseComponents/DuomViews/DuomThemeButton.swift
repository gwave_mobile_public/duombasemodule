//
//  DuomThemeButton.swift
//  Duom
//
//  Created by kuroky on 2022/9/15.
//

import UIKit

public enum ThemeStyle {
    case white(mask: Bool)       // 背景黑色 字体白色
    case normal      // 背景黑色 字体亮黄色
    case highLight   // 背景亮黄色 字体黑色
    case bothYellow  // 背景透明 字体 边框亮黄色
}

public class DuomThemeButton: UIButton {
    public var style: ThemeStyle?
    public var font: UIFont?
    
    public convenience init(style: ThemeStyle, title: String, font: UIFont? = nil) {
        self.init()
        self.style = style
        self.font = font
        
        self.setTitle(title, for: .normal)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        switch self.style {
        case .highLight:
            self.backgroundColor = UIColor.themeColor
            self.setTitleColor(UIColor.black, for: .normal)
            self.titleLabel?.font = font ?? UIFont.appFont(ofSize: 14, fontType: .Inter_Blod)
            self.roundCorners(radius: min(self.bounds.width, self.bounds.height) / 2)
        case .normal:
            self.backgroundColor = UIColor.themeBlackColor
            self.setTitleColor(UIColor.themeColor, for: .normal)
            self.titleLabel?.font =  font ?? UIFont.appFont(ofSize: 14, fontType: .Inter_Blod)
            self.rounded(color: UIColor.themeColor, borderWidth: 1, radius: min(self.bounds.width, self.bounds.height) / 2)
        case let .white(mask):
            self.backgroundColor = UIColor.themeBlackColor
            self.setTitleColor(UIColor.textWhiteColor, for: .normal)
            self.titleLabel?.font =  font ?? UIFont.appFont(ofSize: 16, fontType: .Inter_Blod)
            if mask {
                self.rounded(color: UIColor.textWhiteColor, borderWidth: 1, radius: min(self.bounds.width, self.bounds.height) / 2)
            }
        case .bothYellow:
            self.backgroundColor = .clear
            self.setTitleColor(.themeColor, for: .normal)
            self.titleLabel?.font =  font ?? UIFont.appFont(ofSize: 14, fontType: .Inter_Blod)
            self.rounded(color: .themeColor, borderWidth: 1, radius: min(self.bounds.width, self.bounds.height) / 2)
        case .none:
            break
        }
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.alpha = 0.65
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.alpha = 1
    }
    
    public override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.alpha = 1
    }

}
