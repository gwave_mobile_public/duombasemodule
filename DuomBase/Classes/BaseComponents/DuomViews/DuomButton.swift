//
//  DuomButton.swift
//  DuomBase_Example
//
//  Created by kuroky on 2022/8/31.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import UIKit

@IBDesignable
open class DuomButton: UIButton {
    @objc public enum DuomAlignment: Int {
        /// 上图下文
        case normal
        /// 左文右图
        case leftRightChange
        /// 默认左图右文
        case `default`
    }
    
    /// 仅用于DuomAlignment在xib的映射
    @IBInspectable private var alignmentXib: Int = DuomAlignment.normal.rawValue {
        didSet {
            if let DuomAli = DuomAlignment(rawValue: alignmentXib) {
                duomAlignment = DuomAli
            }
        }
    }
    
    public var duomAlignment: DuomAlignment = .normal
    
    public var padding: CGFloat = 5.0
    
    override open func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.titleRect(forContentRect: contentRect)
        
        let imageRect = super.imageRect(forContentRect: contentRect)
        
        if imageRect == .zero { return rect }
        
        switch duomAlignment {
        case .leftRightChange:
            return CGRect(
                x: (frame.width - rect.width - imageRect.width - padding) * 0.5,
                y: rect.origin.y,
                width: rect.width,
                height: rect.height
            )
        case .normal:
            return CGRect(
                x: 0,
                y: imageRect.maxY - rect.height / 2.0 + padding / 2.0,
                width: contentRect.width,
                height: rect.height
            )
        case .default:
            return CGRect(
                x: (frame.width - rect.width - imageRect.width - padding) * 0.5 + imageRect.width + padding,
                y: rect.origin.y,
                width: rect.width,
                height: rect.height
            )
        }
    }
    
    override open func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.imageRect(forContentRect: contentRect)
        
        let titleRect = self.titleRect(forContentRect: contentRect)
        
        switch duomAlignment {
        case .leftRightChange:
            return CGRect(
                x: (frame.width - rect.width - titleRect.width - padding) * 0.5 + titleRect.width + padding,
                y: rect.origin.y,
                width: rect.width,
                height: rect.height
            )
        case .normal:
            return CGRect(
                x: (contentRect.width - rect.width) / 2.0,
                y: (contentRect.height - titleRect.height - rect.height - padding) / 2.0,
                width: rect.width,
                height: rect.height
            )
        case .default:
            return CGRect(
                x: (frame.width - rect.width - titleRect.width - padding) * 0.5,
                y: rect.origin.y,
                width: rect.width,
                height: rect.height
            )
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        
        if let image = imageView?.image {
            var labelHeight: CGFloat = 0.0
            let labelWidth = contentRect(forBounds: bounds).width
            if let size = titleLabel?.sizeThatFits(CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)) {
                labelHeight = size.height
            }
            
            switch duomAlignment {
            case .normal:
                return CGSize(width: max(size.width, labelWidth, image.size.width), height: image.size.height + labelHeight + padding)
            default:
                return CGSize(width: size.width + padding, height: max(image.size.height, labelHeight, size.height))
            }
        }
        
        return size
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        centerTitleLabel()
        setupUI()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        centerTitleLabel()
        setupUI()
    }
    
    private func centerTitleLabel() {
        titleLabel?.textAlignment = .center
    }
    
    open func setupUI() {}
}

open class DuomCustomBadgeButton: DuomButton {
    public class Config {
        public enum Position {
            case rightTop
        }
        
        public enum Size {
            case fit
            case fixedHeiht(CGFloat)
            case custom(CGSize)
        }
        
        public enum Border {
            case none
            case color(UIColor, CGFloat)
        }
        
        public enum Corner {
            case none
            case fit
            case value(CGFloat)
            case custom((UILabel) -> Void)
        }
        
        public enum Offset {
            case zero
            case custom(CGPoint)
            case sizeRatio(wRatio: CGFloat, hRatio: CGFloat)
        }
        
        public var size: Size = .fit
        public var border: Border = .none
        public var corner: Corner = .fit
        public var offset: Offset = .zero
        public var padding: UIEdgeInsets = .zero
        public var position: Position = .rightTop
        public var font: UIFont = UIFont.systemFont(ofSize: 9)
        public var textColor: UIColor = .white
        public var backgroundColor: UIColor = .red
        
        public init(size: Size = .fit, border: Border = .none, corner: Corner = .fit, offset: Offset = .zero, padding: UIEdgeInsets = .zero, position: Position = .rightTop, font: UIFont = UIFont.systemFont(ofSize: 12), textColor: UIColor = .white, backgroundColor: UIColor = UIColor.themeColor) {
            self.size = size
            self.border = border
            self.corner = corner
            self.offset = offset
            self.padding = padding
            self.position = position
            self.font = font
            self.textColor = textColor
            self.backgroundColor = backgroundColor
        }
    }
    
    public var config: Config = .init() {
        didSet {
            updateConfig()
        }
    }
    
    public let badgeLabel = UILabel(frame: .zero).then {
        $0.isHidden = true
        $0.textAlignment = .center
    }
    
    private(set) var badgeValue: String?
    
    override open var isEnabled: Bool {
        didSet {
            if isEnabled, let badgeValue = badgeValue {
                badgeLabel.isHidden = false
                badgeLabel.text = badgeValue
            } else {
                badgeLabel.isHidden = true
                badgeLabel.text = nil
            }
            
            badgeLabel.sizeToFit()
        }
    }
    
    override open func setupUI() {
        addSubview(badgeLabel)
        padding = 0
        duomAlignment = .default
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        badgeLabel.sizeToFit()
        
        var size: CGSize
        switch config.size {
        case .fit:
            size = badgeLabel.bounds.size.with {
                $0.width = min(bounds.width, $0.width + config.padding.hAxisValue)
                $0.height = min(bounds.height, $0.height + config.padding.vAxisValue)
            }
            size.width = max(size.height, size.width)
        case .fixedHeiht(let height):
            size = .init(
                width: max(badgeLabel.bounds.size.width + config.padding.hAxisValue, height),
                height: height + config.padding.vAxisValue
            )
        case .custom(let s):
            size = s
        }
        
        if case .fit = config.corner {
            badgeLabel.layer.cornerRadius = size.height / 2.f
        }
        
        var center: CGPoint
        
        switch config.position {
        case .rightTop:
            switch config.offset {
            case .zero:
                center = CGPoint(x: bounds.maxX, y: bounds.minY)
            case .custom(let p):
                center = CGPoint(x: bounds.maxX + p.x, y: bounds.minY + p.y)
            case .sizeRatio(let wRatio, let hRatio):
                center = CGPoint(x: bounds.maxX + size.width * wRatio, y: bounds.minY + size.height * hRatio)
            }
        }
        
        badgeLabel.frame.size = size
        badgeLabel.center = center
    }
    
    public func updateConfig() {
        switch config.border {
        case .none:
            badgeLabel.layer.borderColor = nil
            badgeLabel.layer.borderWidth = 0
        case .color(let color, let width):
            badgeLabel.layer.borderColor = color.cgColor
            badgeLabel.layer.borderWidth = width
        }
        
        switch config.corner {
        case .fit:
            break
        case .none:
            badgeLabel.layer.cornerRadius = 0
        case .value(let radius):
            badgeLabel.layer.cornerRadius = radius
        case .custom(let block):
            block(badgeLabel)
        }
        
        badgeLabel.font = config.font
        badgeLabel.textColor = config.textColor
        badgeLabel.layer.backgroundColor = config.backgroundColor.cgColor
        
        setNeedsLayout()
    }
    
    open func settingBadgeValue(value: Int?, max: Int = 99) {
        var valueText: String?
        
        if let value = value, value > 0 {
            let text = value > max ? "\(max)+" : "\(value)"
            valueText = text
        } else {
            valueText = nil
        }
        
        badgeValue = valueText
        
        if let badgeValue = badgeValue {
            badgeLabel.isHidden = false
            badgeLabel.text = badgeValue
        } else {
            badgeLabel.isHidden = true
            badgeLabel.text = nil
        }
        
        badgeLabel.sizeToFit()
    }
}

