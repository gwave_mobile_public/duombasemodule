//
//  DuomBaseRefreshFooter.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import MJRefresh
import UIKit

open class DuomBaseRefreshFooter: MJRefreshFooter {
    // 当底部控件出现多少时就自动刷新(默认为1.0，也就是底部控件完全出现时，才会自动刷新)
    open var triggerAutomaticallyRefreshPercent: CGFloat = defaultRefreshPercent
    
    public static var defaultRefreshPercent = -UIScreen.main.bounds.height / 4.f / MJRefreshFooterHeight
    
    open var isOneNewPan: Bool = false
    
    public let loadingView = UIActivityIndicatorView(style: .gray).then {
        $0.hidesWhenStopped = true
    }
    
    public let stateLabel = UILabel(frame: .zero).then {
        $0.textAlignment = .center
        $0.backgroundColor = .clear
    }
    
    @objc open override var state: MJRefreshState {
        didSet {
            if state == .refreshing {
                executeRefreshingCallback()
            } else if state == .noMoreData || state == .idle, oldValue == .refreshing {
                endRefreshingCompletionBlock?()
            }
        }
    }
    
    open override var isHidden: Bool {
        didSet {
            if !oldValue, isHidden {
                state = .idle
            }
        }
    }
    
    open override func prepare() {
        super.prepare()
        
        addSubview(loadingView)
        addSubview(stateLabel)
    }
    
    open override func placeSubviews() {
        super.placeSubviews()
        
        stateLabel.sizeToFit()
        loadingView.sizeToFit()
        
        var loadingCenterX = mj_w * 0.5
        
        loadingCenterX -= stateLabel.frame.width * 0.5 + 8
        
        loadingView.center = CGPoint(x: loadingCenterX, y: mj_h * 0.5)
        
        stateLabel.frame = .init(
            x: loadingView.frame.maxX + 8,
            y: (mj_h - stateLabel.frame.height) / 2.f,
            width: stateLabel.frame.width,
            height: stateLabel.frame.height
        )
    }
    
    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        scrollViewContentSizeDidChange([:])
    }
    
    open override func scrollViewContentSizeDidChange(_ change: [AnyHashable : Any]?) {
        super.scrollViewContentSizeDidChange(change)
        
        guard let scrollView = scrollView else { return }
        
        mj_y = max(scrollView.mj_h, scrollView.mj_contentH) + scrollView.mj_insetB + self.ignoredScrollViewContentInsetBottom
    }
    
    open override func scrollViewPanStateDidChange(_ change: [AnyHashable : Any]?) {
        super.scrollViewPanStateDidChange(change)
        
        if state == .idle, scrollView?.panGestureRecognizer.state == .began {
            isOneNewPan = true
        }
    }
    
    open override func beginRefreshing() {
        guard isOneNewPan else { return }
        
        super.beginRefreshing()
        
        isOneNewPan = false
    }
    
    func criticalOffsetY() -> CGFloat {
        guard let scrollView = scrollView else { return 0 }
        
        if scrollView.mj_contentH + scrollView.mj_insetT + scrollView.mj_insetB > scrollView.mj_h {
            // 内容超出一屏
            return scrollView.mj_contentH + scrollView.mj_insetT + scrollView.mj_insetB - scrollView.mj_h
        } else {
            // 内容不超出一屏
            return 0
        }
    }
    
    func checkShouldTriggerRefresh() -> Bool {
        guard let scrollView = scrollView else { return false }
        
        return scrollView.mj_offsetY > criticalOffsetY() + mj_h * triggerAutomaticallyRefreshPercent
    }
    
}
