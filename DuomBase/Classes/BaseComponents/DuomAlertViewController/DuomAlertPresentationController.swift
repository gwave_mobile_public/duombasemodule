//
//  DuomAlertPresentationController.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/15.
//

import UIKit

internal final class DuomAlertPresentationController: UIPresentationController {
    var dimmingView: UIView?
    var dimmingViewDidTapped: (() -> Void)?
    
    var presentationWrappingView: UIView?
    
    override var presentedView: UIView? {
        return presentationWrappingView
    }
    
    var enableGesture: Bool = true
    
    convenience init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, enableGesture: Bool = true) {
        self.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        self.enableGesture = enableGesture
    }
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    
    override func presentationTransitionWillBegin() {
        
        guard let presentedViewControllerView = super.presentedView else { return }
        
        // presentationWrapperView              <- shadow
        //   |- presentationRoundedCornerView   <- rounded corners (masksToBounds)
        //        |- presentedViewControllerView (presentedViewController.view)
        
        let presentationWrapperView = UIView(frame: frameOfPresentedViewInContainerView).then {
            $0.backgroundColor = UIColor.clear
            $0.layer.shadowColor = UIColor.black.cgColor
            $0.layer.shadowRadius = 5
            $0.layer.shadowOpacity = 0.4
            $0.layer.shadowOffset = CGSize(width: 0, height: 0)
            $0.layer.cornerRadius = 10
        }
        
        let presentationRoundedCornerView = UIView(frame: presentationWrapperView.bounds).then {
            $0.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            $0.backgroundColor = .clear
            $0.layer.cornerRadius = 12
            $0.layer.masksToBounds = true
        }
        
        presentedViewControllerView.frame = presentationWrapperView.bounds
        presentedViewControllerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        presentationWrapperView.addSubview(presentationRoundedCornerView)
        presentationRoundedCornerView.addSubview(presentedViewControllerView)
        
        self.presentationWrappingView = presentationWrapperView
        
        guard let containerView = containerView else { return }
        
        dimmingView = UIView(frame: containerView.bounds).then {
            $0.alpha = 0
            $0.isOpaque = false
            $0.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dimmingViewTapped(_:))))
        }
        
        containerView.insertSubview(dimmingView!, at: 0)
        
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView?.alpha = 1.0
        }, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView?.alpha = 0.0
        }, completion: nil)
    }
    
    @objc func dimmingViewTapped(_ sender: UITapGestureRecognizer) {
        if enableGesture {
            dimmingViewDidTapped?()
            presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
}

extension DuomAlertPresentationController {
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            dimmingView = nil
            presentationWrappingView = nil
        }
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            dimmingView = nil
            presentationWrappingView = nil
        }
    }
}

// MARK: Layout

extension DuomAlertPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerViewBounds = containerView?.bounds else { return .zero }

        return calculateScreenCenterRect(containerViewBounds)
    }
    
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        
        guard let containerView = containerView else { return }
        
        dimmingView?.frame = containerView.bounds
        
        presentationWrappingView?.frame = frameOfPresentedViewInContainerView
    }
    
    func calculateScreenCenterRect(_ containerViewBounds: CGRect) -> CGRect {
        let width = containerViewBounds.width * 0.88
        let height = max(presentedViewController.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height, 160)
        let y = (containerViewBounds.height - height) * 0.5 - 20.0
        let x = (containerViewBounds.width - width) * 0.5
        
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    func calculateViewCenterRect(_ containerViewBounds: CGRect) -> CGRect {
        var topOffset: CGFloat = 0
        var bottomOffset: CGFloat = 0
        
        if !UIApplication.shared.isStatusBarHidden {
            topOffset += UIApplication.shared.statusBarFrame.height
        }
        
        if let presentingViewController = presentingViewController as? UITabBarController {
            
            if let selectedViewController = presentingViewController.selectedViewController {
                
                if let selectedViewController = selectedViewController as? UINavigationController {
                    if !selectedViewController.navigationBar.isHidden {
                        topOffset += selectedViewController.navigationBar.frame.height
                    }
                } else {
                    if let navigationBar = selectedViewController.navigationController?.navigationBar, !navigationBar.isHidden {
                        topOffset += navigationBar.frame.height
                    }
                }
            }
            
            bottomOffset += presentingViewController.tabBar.frame.height
        } else if let presentingViewController = presentingViewController as? UINavigationController {
            if !presentingViewController.navigationBar.isHidden {
                topOffset += presentingViewController.navigationBar.frame.height
            }
            
            if let tabBar = presentingViewController.tabBarController?.tabBar, !tabBar.isHidden {
                bottomOffset += tabBar.frame.height
            }
        }
        
        let width = containerViewBounds.width * 0.88
        let height = max(presentedViewController.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height, 160)
        let y = (containerViewBounds.height - topOffset - bottomOffset) * 0.5 + topOffset - height * 0.5
        let x = (containerViewBounds.width - width) * 0.5
        
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
}
