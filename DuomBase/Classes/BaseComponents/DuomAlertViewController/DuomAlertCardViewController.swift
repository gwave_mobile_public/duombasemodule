//
//  DuomAlertCardViewController.swift
//  DuomBase
//
//  Created by kuroky on 2023/6/18.
//

import Foundation
import SwiftyImage
import UIKit

public struct DuomAlertCardItemConfig {
    public var title: String
    public var dismissOnTap: Bool
    public var font: UIFont
    public var titleColor: UIColor
    public var backgroundColor: UIColor
    public var highlightedColor: UIColor
    public init(title: String, dismissOnTap: Bool = true, font: UIFont = .appFont(ofSize: 20, fontType: .TT_Bold), titleColor: UIColor = .white, backgroundColor: UIColor, highlightedColor: UIColor) {
        self.title = title
        self.dismissOnTap = dismissOnTap
        self.font = font
        self.titleColor = titleColor
        self.backgroundColor = backgroundColor
        self.highlightedColor = highlightedColor
    }
}

open class DuomAlertCardViewController: UIViewController, UIGestureRecognizerDelegate {
    public static let cardAlertMinHeight: CGFloat = 170.0
    public static let minContentHeight: CGFloat = 22.0
    
    public var insideAction: (() -> Void)?
    
    public var outsideAction: (() -> Void)?
    
    public var buttonContainer = UIView(backgroundColor: .clear)
    
    internal var presentationManager: DuomAlertPresentationManager
    
    private lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(containerViewDidTap(_:))).then {
        $0.delegate = self
    }
    
    public var topContainerView: UIView = UIView().then {
        $0.backgroundColor = .white
    }
    
    public var titleLabel: UILabel = UILabel().then {
        $0.textColor = UIColor(6, 5, 6)
        $0.font = .appFont(ofSize: 24, fontType: .CR_Regular)
        $0.textAlignment = .left
    }
    
    public var messageLabel: UILabel = UILabel().then {
        $0.textColor = UIColor(6, 5, 6)
        $0.font = .appFont(ofSize: 18, fontType: .TT_Regular)
        $0.numberOfLines = 0
        $0.lineBreakMode = .byWordWrapping
        $0.textAlignment = .left
    }
    
    private var cornerView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "inside_corner")
    }
    
    private lazy var insideButton: UIButton = UIButton()
    
    private lazy var outsideButton: UIButton = UIButton()
    
    private(set) var contentHeight: CGFloat = 36.DScale
    
    // 单个按钮？
    private(set) var isSingleItem: Bool = true
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(transitionStyle: DuomAlertTransitionStyle = .fadeIn,
                      enableGesture: Bool = true,
                      title: String = "",
                      content: String = "",
                    fixHeight: CGFloat? = nil,
                insideItem: DuomAlertCardItemConfig? = DuomAlertCardItemConfig(title: "Discard", dismissOnTap: true, font: .appFont(ofSize: 20, fontType: .TT_Regular), titleColor: UIColor(6, 5, 6), backgroundColor: .clear, highlightedColor: .clear),
                      outsideItem: DuomAlertCardItemConfig = DuomAlertCardItemConfig(title: "CANCEL", dismissOnTap: true, font: .appFont(ofSize: 20, fontType: .TT_Bold), titleColor: .white, backgroundColor: UIColor(218, 42, 42, 1), highlightedColor: UIColor(255, 70, 70, 1))) {
        presentationManager = DuomAlertPresentationManager(transitionStyle: transitionStyle, enableGesture: enableGesture, isCard: true)

        super.init(nibName: nil, bundle: nil)
        
        transitioningDelegate = presentationManager
        
        modalPresentationStyle = .custom
        
        modalPresentationCapturesStatusBarAppearance = true
        
        titleLabel.text = title
        
        let contentAttritext = content.attributes {
            $0.font(.appFont(ofSize: 18, fontType: .TT_Regular)).textColor(UIColor(6, 5, 6)).lineBreakMode(.byWordWrapping).lineHeight(18).lineHeightMultiple(0.84)
        }
        messageLabel.attributedText = contentAttritext
        
        if let fixHeight = fixHeight {
            self.contentHeight = fixHeight
        } else {
            let attrHeight = contentAttritext.boundingRect(with: CGSize(width: UIScreen.screenWidth - 64 * 2, height: .infinity), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).height
            self.contentHeight = max(DuomAlertCardViewController.minContentHeight, ceil(attrHeight))
        }
        
        if let insideItem = insideItem {
            isSingleItem = false
            insideButton.setAttributedTitle(insideItem.title.attributes {
                $0.underline(.single, color: UIColor(6, 5, 6)).textColor(UIColor(6, 5, 6))
            }, for: .normal)
            insideButton.setAttributedTitle(insideItem.title.attributes {
                $0.underline(.single, color: UIColor(6, 5, 6).withAlphaComponent(0.5)).textColor(UIColor(6, 5, 6).withAlphaComponent(0.5))
            }, for: .highlighted)
            insideButton.titleLabel?.font = insideItem.font
            insideButton.addTarget(self, action: #selector(insideButtonAction(_:)), for: .touchUpInside)
        }
        insideButton.backgroundColor = .white
        
        outsideButton.setTitle(outsideItem.title, for: .normal)
        outsideButton.setTitleColor(outsideItem.titleColor, for: .normal)
        outsideButton.setTitleColor(outsideItem.titleColor.withAlphaComponent(0.5), for: .highlighted)
        outsideButton.titleLabel?.font = outsideItem.font
        outsideButton.titleLabel?.lineBreakMode = .byTruncatingTail
        outsideButton.setBackgroundImage(UIImage.resizable().color(outsideItem.backgroundColor).corner(topLeft: 12, topRight: 27, bottomLeft: 12, bottomRight: 27).image, for: .normal)
        outsideButton.setBackgroundImage(UIImage.resizable().color(outsideItem.highlightedColor).corner(topLeft: 12, topRight: 27, bottomLeft: 12, bottomRight: 27).image, for: .highlighted)
        outsideButton.addTarget(self, action: #selector(outsideButtonAction(_:)), for: .touchUpInside)
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let containerView = presentationController?.containerView {
            containerView.removeGestureRecognizer(tapGesture)
            containerView.addGestureRecognizer(tapGesture)
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        
        view.addSubview(topContainerView)
        topContainerView.addSubview(titleLabel)
        topContainerView.addSubview(messageLabel)
        view.addSubview(buttonContainer)
        buttonContainer.addSubview(insideButton)
        buttonContainer.addSubview(cornerView)
        buttonContainer.addSubview(outsideButton)
        
        let dynamicHeight = DuomAlertCardViewController.cardAlertMinHeight + self.contentHeight - DuomAlertCardViewController.minContentHeight
        
        topContainerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(dynamicHeight - 62)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(32 - 1.DScale)
            make.leading.equalTo(17)
            make.height.equalTo(25.DScale)
        }
        
        messageLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(self.contentHeight)
            make.bottom.equalTo(-16)
        }
        
        buttonContainer.snp.makeConstraints { make in
            make.top.equalTo(topContainerView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(62)
        }
        
        insideButton.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.width.equalTo(isSingleItem ? 72.DScale :  131.DScale)
        }
        cornerView.snp.makeConstraints { make in
            make.leading.equalTo(insideButton.snp.trailing)
            make.top.equalToSuperview()
            make.size.equalTo(CGSize(width: 16, height: 16))
        }
    
        outsideButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalTo(8)
            make.leading.equalTo(insideButton.snp.trailing).offset(9)
        }
        
        insideButton.setupCorners(rect: CGRect(x: 0, y: 0, width: isSingleItem ? 72.DScale : 131.DScale, height: 62), corners: [.bottomLeft, .bottomRight], radii: CGSize(width: 16, height: 16))
        topContainerView.setupCorners(rect: CGRect(x: 0, y: 0, width: UIScreen.screenWidth - 48 * 2, height: 87 + self.contentHeight), corners: [.topLeft, .topRight, .bottomRight], radii: CGSize(width: 16, height: 16))
    }
    
 
    
    @objc open func containerViewDidTap(_ sender: UITapGestureRecognizer) {}
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
    
    @objc open func insideButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: { [weak self] in
            self?.insideAction?()
        })
    }

    @objc open func outsideButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: { [weak self] in
            self?.outsideAction?()
        })
    }
    
}
 
