//
//  DuomPagingContainerView.swift
//  Differentiator
//
//  Created by kuroky on 2022/11/29.
//

import Foundation

// MARK: DuomPagingContainerViewDelegate
public protocol DuomPagingContainerViewDelegate: AnyObject {
    ///  滑动回调
    /// - Parameter containerView: containerView description
    func containerViewDidScroll(containerView: DuomPagingContainerView)
    
    /// 切换回调
    /// - Parameters:
    ///   - containerView: containerView description
    ///   - fromIndex: fromIndex description
    ///   - toIndex: toIndex description
    ///   - percent: 滑动进度(可以根据进度，自行调用addSubview，选择添加view的时机，默认是滑动停止)
    func containerView(_ containerView: DuomPagingContainerView, from fromIndex: Int, to toIndex: Int, percent: CGFloat)
    
    ///  停止回调
    /// - Parameters:
    ///   - containerView: containerView description
    ///   - item: item description
    ///   - index:  停止的下标
    func containerView(_ containerView: DuomPagingContainerView, item: DuomPagingContainerItem, stopAt index: Int)
    
    /// 试图添加回调
    /// - Parameters:
    ///   - containerView: containerView description
    ///   - item: 添加的视图
    ///   - index: 添加的下标
    func containerView(_ containerView: DuomPagingContainerView, item: DuomPagingContainerItem, addAt index: Int)
    
    /// 删除视图回调
    /// - Parameters:
    ///   - containerView: containerView description
    ///   - item: 删除的item
    ///   - index: 删除的下标
    func containerView(_ containerView: DuomPagingContainerView, item: DuomPagingContainerItem, removedAt index: Int)
}

public extension DuomPagingContainerViewDelegate {
    func containerViewDidScroll(containerView: DuomPagingContainerView) {}
    func containerView(_ containerView: DuomPagingContainerView, from fromIndex: Int, to toIndex: Int, percent: CGFloat) {}
    func containerView(_ containerView: DuomPagingContainerView, item: DuomPagingContainerItem, stopAt index: Int) {}
    func containerView(_ containerView: DuomPagingContainerView, item: DuomPagingContainerItem, addAt index: Int) {}
    func containerView(_ containerView: DuomPagingContainerView, item: DuomPagingContainerItem, removedAt index: Int) {}
}

// MARK: DuomPagingContainerViewDataSource
public protocol DuomPagingContainerViewDataSource: AnyObject {
    /// 数据源
    /// - Parameter containerView: containerView description
    /// - Returns: count
    func numberOfItems(in containerView: DuomPagingContainerView) -> Int
    
    /// 获取index对应item
    /// - Parameters:
    ///   - containerView: containerView description
    ///   - index: index description
    /// - Returns: description
    func containerView(_ containerView: DuomPagingContainerView, itemAt index: Int) -> DuomPagingContainerItem?
    
    /// 需要删除时，判断是否需要删除(如超过最大加载数量，而有些页面需要常驻)
    /// - Parameters:
    ///   - containerView: containerView description
    ///   - index: index description
    /// - Returns: description
    func itemWillRemove(of containerView: DuomPagingContainerView, at index: Int) -> Bool
}

public extension DuomPagingContainerViewDataSource {
    func itemWillRemove(of containerView: DuomPagingContainerView, at index: Int) -> Bool {
        return true
    }
}

// MARK: Item
public protocol DuomPagingContainerItem {
    var itemView: UIView { get }
    var itemViewController: UIViewController? { get }
}

public extension DuomPagingContainerItem where Self: UIView {
    var itemView: UIView { self }
    var itemViewController: UIViewController? { nil }
}

public extension DuomPagingContainerItem where Self: UIViewController {
    var itemView: UIView { self.view }
    var itemViewController: UIViewController? { self }
}

// MARK: DuomPagingContainerGestureRecognizerDelegate
public protocol DuomPagingContainerGestureRecognizerDelegate: AnyObject {
    func duomPagingContainerScrollView(_ scrollView: UIScrollView, gestureRecognizerShouldBegin gestureRecognizer: UIGestureRecognizer) -> Bool
    func duomPagingContainerScrollView(_ scrollView: UIScrollView, gestureRecognizer: UIGestureRecognizer, shouldRecognizerSimulataneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
}

public extension DuomPagingContainerGestureRecognizerDelegate {
    func duomPagingContainerScrollView(_ scrollView: UIScrollView, gestureRecognizerShouldBegin gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func duomPagingContainerScrollView(_ scrollView: UIScrollView, gestureRecognizer: UIGestureRecognizer, shouldRecognizerSimulataneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
}

// MARK: ContainerScrollView
public class ContainerScrollView: UIScrollView, UIGestureRecognizerDelegate {
    public weak var gestureDelegate: DuomPagingContainerGestureRecognizerDelegate?
    
    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        gestureDelegate?.duomPagingContainerScrollView(self, gestureRecognizerShouldBegin: gestureRecognizer) ?? true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        gestureDelegate?.duomPagingContainerScrollView(self, gestureRecognizer: gestureRecognizer, shouldRecognizerSimulataneouslyWith: otherGestureRecognizer) ?? false
    }
}
 
open class DuomPagingContainerView: UIView {
    /// 删除策略枚举
    public enum RemoveStrategy {
        /// 删除最早加入的
        case earlist
        /// 删除距离当前最远的
        case farthest
    }
    
    public weak var delegate: DuomPagingContainerViewDelegate?
    public weak var dataSource: DuomPagingContainerViewDataSource?
    
    public private(set) lazy var scrollView: ContainerScrollView = {
        let scrollView = ContainerScrollView()
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        return scrollView
    }()
    
    /// 允许滚动 默认true
    public var scrollEnable: Bool = true {
        didSet {
            scrollView.isScrollEnabled = scrollEnable
        }
    }
    /// 默认下标
    public var defaultSelectedIndex: Int = 0
    /// 当前选中下标
    public private(set) var selectedIndex: Int = 0
    /// 最大加载数量，0按照最大加载处理
    public var maxExistCount: Int = 0
    /// 当前加入的item
    public private(set) var items: [Int: DuomPagingContainerItem] = [:]
    /// 删除策略
    public var removeStrategy: RemoveStrategy = .earlist
    
    public var gestureDelegate: DuomPagingContainerViewDelegate?
    
    fileprivate var itemIndexs: [Int] = []
    fileprivate var isFirstLayout: Bool = true
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        config()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func config()  {
        addSubview(scrollView)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        self.scrollView.frame = self.bounds
        if isFirstLayout {
            isFirstLayout = false
            configScrollViewContenSize()
            scroll(toIndex: defaultSelectedIndex, animated: false)
        } else {
            configScrollViewContenSize()
            resetItems()
            scroll(toIndex: selectedIndex, animated: false)
        }
    }
    
    // MARK: Public Method
    // 刷新
    public func reloadData(selectedAt index: Int = 0) {
        guard isFirstLayout == false else {
            self.defaultSelectedIndex = index
            return
        }
        configScrollViewContenSize()
        self.removeAllItem()
        self.scroll(toIndex: index, animated: false)
    }
    
    public func item(atIndex index: Int) -> DuomPagingContainerItem? {
        return self.items[index]
    }
    
    public func currentItem() -> DuomPagingContainerItem? {
        return self.item(atIndex: self.selectedIndex)
    }
    
    public func addSubView(at index: Int) {
        guard items[index] == nil,
              let item = self.dataSource?.containerView(self, itemAt: index) else {
            return
        }
        
        self.scrollView.addSubview(item.itemView)
        item.itemView.frame = CGRect(x: CGFloat(index) * bounds.size.width, y: 0, width: bounds.size.width, height: bounds.size.height)
  
        self.items[index] = item
        self.itemIndexs.append(index)
        
        self.delegate?.containerView(self, item: item, addAt: index)
        self.delegate?.containerView(self, item: item, stopAt: index)
    }
    
    public func scroll(toIndex index: Int, animated: Bool = false) {
        guard let count = self.dataSource?.numberOfItems(in: self), index < count, self.frame.width > 0 else {
            return
        }
        let contentOffset_x = CGFloat(index) * self.frame.width
        self.setContentOffset(CGPoint(x: contentOffset_x, y: 0), animated: animated)
    }
    
    public func setContentOffset(_ contentOffset: CGPoint, animated: Bool) {
        self.scrollView.setContentOffset(contentOffset, animated: animated)
        if !animated {
            self.scrollViewEndScroll(self.scrollView)
        }
    }
    
    fileprivate func configScrollViewContenSize() {
        let itemCount = dataSource?.numberOfItems(in: self) ?? 0
        let targetSize = CGSize(width: CGFloat(itemCount) * bounds.size.width, height: bounds.size.height)
        scrollView.contentSize = targetSize
    }
    
    fileprivate func addSubview(at contentOffset: CGPoint) {
        guard self.frame.width > 0 else {
            return
        }
        let index = Int(contentOffset.x / self.frame.width)
        selectedIndex = index
        addSubView(at: index)
//        checkToRemove()
    }
}

// MARK: DuomPagingContainerView--> Private Method
fileprivate extension DuomPagingContainerView {
    func resetItems() {
        for index in items.keys {
            guard let item = items[index] else {
                continue
            }
            let itemFrame = CGRect(x: CGFloat(index) * bounds.size.width, y: 0, width: bounds.size.width, height: bounds.size.height)
            item.itemView.frame = itemFrame
        }
    }
    
    /// 删除全部
    func removeAllItem() {
        for index in items.keys {
            guard let item = items[index] else {
                continue
            }
            if let view = item as? UIView {
                view.removeFromSuperview()
            } else if let vc = item as? UIViewController {
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
            self.delegate?.containerView(self, item: item, removedAt: index)
        }
        
        self.items.removeAll()
        self.itemIndexs.removeAll()
    }
    
    /// 检测是否需要删除
    func checkToRemove() {
        switch removeStrategy {
        case .earlist:
            removeEarliestItem()
        case .farthest:
            removeFarthestItem()
        }
    }
    
    /// 删除最早加入的
    func removeEarliestItem() {
        guard maxExistCount > 0,
              itemIndexs.count > self.maxExistCount,
              let dataSource = self.dataSource else {
            return
        }
        for (index, itemIndex) in itemIndexs.enumerated() where itemIndex != selectedIndex && dataSource.itemWillRemove(of: self, at: itemIndex) {
            removeItem(at: itemIndex)
            itemIndexs.remove(at: index)
            break
        }
    }
    
    /// 删除离当前位置最远的
    func removeFarthestItem() {
        guard maxExistCount > 0,
              itemIndexs.count > self.maxExistCount,
              let dataSource = self.dataSource else {
            return
        }
        
        var farstItemIndex: Int?
        var index: Int?
        var maxDistance = 0
        for (i, itemIndex) in self.itemIndexs.enumerated() where itemIndex != selectedIndex && dataSource.itemWillRemove(of: self, at: itemIndex) {
            let distance = abs(itemIndex - self.selectedIndex)
            if distance > maxDistance {
                maxDistance = distance
                farstItemIndex = itemIndex
                index = i
            }
        }

        if let index = index,
            let farstItemIndex = farstItemIndex {
            removeItem(at: farstItemIndex)
            itemIndexs.remove(at: index)
        }
    }
    
    func removeItem(at index: Int) {
        guard let item = self.items[index] else {
            return
        }
        
        item.itemView.removeFromSuperview()
        item.itemViewController?.removeFromParent()

        self.delegate?.containerView(self, item: item, removedAt: index)
        self.items[index] = nil
    }
}

// MARK: DuomPagingContainerView->UIScrollViewDelegate
extension DuomPagingContainerView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.containerViewDidScroll(containerView: self)
        
        guard let count = self.dataSource?.numberOfItems(in: self), self.scrollView.contentSize.width > 0 else {
            return
        }
        
        let maxOffsetX = CGFloat(count) * scrollView.frame.width
        let contentOffsetX = max(0, min(scrollView.contentOffset.x, maxOffsetX))
        
        let selectedIndex = self.selectedIndex
        var percent = (contentOffsetX - CGFloat(selectedIndex) * scrollView.frame.width)/scrollView.frame.width
        var toIndex: Int = selectedIndex

        if percent < 0 && percent > -1 {
            if self.selectedIndex == 0 { return }
            toIndex = max(0, selectedIndex - 1)
        } else if percent > 0 && percent < 1 {
            if self.selectedIndex == count { return }
            toIndex = min(count, selectedIndex + 1)
        } else {
            toIndex = min(count, selectedIndex + Int(percent))
            percent = percent < 0 ? -1 : 1
            self.selectedIndex = toIndex
        }
        self.delegate?.containerView(self, from: selectedIndex, to: toIndex, percent: percent)
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewEndScroll(scrollView)
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.scrollViewEndScroll(scrollView)
    }
    
    fileprivate func scrollViewEndScroll(_ scrollView: UIScrollView) {
        if let item = items[selectedIndex] {
            self.delegate?.containerView(self, item: item, stopAt: selectedIndex)
        }
        self.addSubview(at: scrollView.contentOffset)
    }
}
