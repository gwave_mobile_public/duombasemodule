//
//  DuomSegmentIndicatorBaseView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/28.
//

import Foundation

// 指示器
open class DuomSegmentIndicatorBaseView: UIView {
    /// 所在父视图的bounds
    internal var superBounds: CGRect = .zero
    
    /// 点击选中
    open func selected(to toRect: CGRect, animation: Bool) {}
    
    /// 滑动选中
    open func scroll(from fromRect: CGRect, to toRect: CGRect, progress: CGFloat)  {}
}
