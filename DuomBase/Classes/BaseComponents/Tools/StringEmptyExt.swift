//
//  StringEmptyExt.swift
//  DuomBase
//
//  Created by kuroky on 2022/10/21.
//

import Foundation

public extension Optional where Wrapped == String {
    var nilOrEmpty: String? {
        guard let strongSelf = self else {
            return nil
        }
        return strongSelf.isEmpty ? nil : strongSelf
    }

    var isNilOrEmpty: Bool {
        return nilOrEmpty == nil
    }
}

public extension String {
    var nilOrEmpty: String? {
        return isEmpty ? nil : self
    }
}
