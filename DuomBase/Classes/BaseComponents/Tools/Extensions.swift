//
//  Color+Extension.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import UIKit

extension UIViewController {
    public func addViewTop(_ view: UIView) -> UIView {
            if let window: UIWindow = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first {
                window.addSubview(view)
                return window
            } else {
                self.view.addSubview(view)
                return self.view
            }
        }

}


public extension NotificationCenter {
//    func observerKeyboard(listening: (((endFrame: CGRect, duration: Double)) -> Void)? = nil) {
//        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification, object: nil, queue: nil) { (notify) in
//            guard let userInfo = notify.userInfo else {
//                return
//            }
//            
//            let endKeyboardFrameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
//            let endKeyboardFrame = endKeyboardFrameValue?.cgRectValue
//            let durationValue = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
//            let duration = durationValue?.doubleValue
//            
//            if let listening = listening {
//                let callbackParameter = (endFrame: endKeyboardFrame!, duration: duration!)
//                listening(callbackParameter)
//            }
//        }
//    }
}

public extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
      
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    class func getWindow() -> UIWindow? {
        var window: UIWindow?
        if #available(iOS 13.0, *) {
//            window = self.shared.connectedScenes
//                .filter { $0.activationState == .foregroundActive }
//                .map { $0 as? UIWindowScene }
//                .compactMap { $0 }
//                .last?.windows
//                .last
            window = UIApplication.shared.connectedScenes
                .filter { $0.activationState == .foregroundActive }
                .compactMap { $0 as? UIWindowScene }
                .first?
                .windows
                .filter { $0.isKeyWindow }
                .first
        } else {
            window = self.shared.keyWindow
        }
        return window
    }
}

