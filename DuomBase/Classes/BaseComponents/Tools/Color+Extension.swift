//
//  Color+Extension.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import UIKit
 
public extension UIColor {
     
    //使用rgb方式生成自定义颜色
    convenience init(_ r : CGFloat, _ g : CGFloat, _ b : CGFloat)
    {
        let red = r / 255.0
        let green = g / 255.0
        let blue = b / 255.0
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
     
    //使用rgba方式生成自定义颜色
    convenience init(_ r : CGFloat, _ g : CGFloat, _ b : CGFloat, _ a : CGFloat)
    {
        let red = r / 255.0
        let green = g / 255.0
        let blue = b / 255.0
        self.init(red: red, green: green, blue: blue, alpha: a)
    }
    
    //16进制生成自定义颜色
    convenience init(hex: String, alpha: CGFloat? = nil) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(
            red:   CGFloat(r) / 255,
            green: CGFloat(g) / 255,
            blue:  CGFloat(b) / 255,
            alpha: alpha ?? CGFloat(a) / 255
        )
    }
    
    convenience init(hex: UInt) {
        if hex > 0xFFFFFF {
            self.init(
                red: CGFloat((hex & 0xFF00_0000) >> 24) / 255.0,
                green: CGFloat((hex & 0x00FF_0000) >> 16) / 255.0,
                blue: CGFloat((hex & 0x0000_FF00) >> 8) / 255.0,
                alpha: CGFloat((hex & 0x0000_00FF) >> 0) / 255.0
            )
        } else {
            self.init(
                red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(hex & 0x0000FF) / 255.0,
                alpha: 1
            )
        }
    }

    convenience init(hex: UInt, alpha: CGFloat) {
        self.init(
            red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hex & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
}


public extension UIColor {
    /// 亮黄色
    static let themeColor = UIColor(206, 218, 57)

    static let themeBlackColor = UIColor(27, 27, 27)
    
    static let themeBlackColor65 = UIColor(65, 65, 65)

    static let themeRedColor = UIColor(194, 0, 0)

    static let themeLineColor = UIColor(55, 55, 55)

    static let textWhiteColor = UIColor(255, 255, 255)

    static let textLightColor = UIColor(165, 165, 165)

    static let grayColor = UIColor(131, 131, 131)
}
