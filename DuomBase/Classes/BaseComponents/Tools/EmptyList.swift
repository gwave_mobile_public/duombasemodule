//
//  EmptyList.swift
//  DuomBase
//
//  Created by kuroky on 2022/10/19.
//

import Foundation
import UIKit
import RxSwift

public protocol EmptyAble where Self: UIView {
    typealias FilterHandler = (() -> Bool)
    // 自定义过滤器
    var customFilter: FilterHandler? { get set }
    
    var edgeInsets: UIEdgeInsets { get set }
}

private extension DispatchQueue {
    private static var _onceTracker = [String]()
    class func _once(token: String, closure: () -> Void) {
        objc_sync_enter(self)
        defer {
            objc_sync_exit(self)
        }
        if _onceTracker.contains(token) {
            return
        }
        _onceTracker.append(token)
        closure()
    }
}

private extension NSObject {
    static func _exchangeSelector(_ original: Selector, by newSelector: Selector) {
        guard let originalMethod = class_getInstanceMethod(self, original),
            let newMethod = class_getInstanceMethod(self, newSelector) else { return }
        let didAddMethod = class_addMethod(self, original, method_getImplementation(newMethod), method_getTypeEncoding(newMethod))
        if didAddMethod {
            class_replaceMethod(self, newSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
        } else {
            method_exchangeImplementations(originalMethod, newMethod)
        }
    }
}

private var emptyKey_t: Void?
private var checker_t: Void?
private let swizzleKey_t: String = "EmptyTableView_Key"

extension UITableView {
    public var emptyView: EmptyAble? {
        get {
            return objc_getAssociatedObject(self, &emptyKey_t) as? EmptyAble
        }
        set {
            UITableView.swizzling()
            (objc_getAssociatedObject(self, &emptyKey_t) as? EmptyAble)?.removeFromSuperview()
            objc_setAssociatedObject(self, &emptyKey_t, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            emptyAbleChecker.checkIfEmpty(newValue)
        }
    }
    
    private var emptyAbleChecker: EmptyAbleChecker {
        if let old = objc_getAssociatedObject(self, &checker_t) as? EmptyAbleChecker {
            return old
        }
        let new = EmptyAbleChecker(parentView: self, isEmpty: { [weak self] () -> Bool in
            guard let self = self, let dataSource = self.dataSource else { return true }
            let numberOfSections = dataSource.numberOfSections?(in: self) ?? 1
            return !(0 ..< numberOfSections).contains {
                dataSource.tableView(self, numberOfRowsInSection: $0) > 0
            }
        })
        objc_setAssociatedObject(self, &checker_t, new, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        return new
    }
    
    private static func swizzling() {
        DispatchQueue._once(token: swizzleKey_t) {
            _exchangeSelector(#selector(UITableView.reloadData), by: #selector(UITableView.swizzled_reloadData))
        }
    }
    
    @objc private func swizzled_reloadData() {
        swizzled_reloadData()
        emptyAbleChecker.checkIfEmpty(emptyView)
    }
}


private var emptyKey_c: Void?
private var checker_c: Void?
private let swizzleKey_c: String = "EmptyCollection_Key"

extension UICollectionView {
    public var emptyView: EmptyAble? {
        get {
            return objc_getAssociatedObject(self, &emptyKey_c) as? EmptyAble
        }
        set {
            UICollectionView.swizzling()
            (objc_getAssociatedObject(self, &emptyKey_c) as? UIView)?.removeFromSuperview()
            objc_setAssociatedObject(self, &emptyKey_c, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            emptyAbleChecker.checkIfEmpty(newValue)
        }
    }

    private var emptyAbleChecker: EmptyAbleChecker {
        if let old = objc_getAssociatedObject(self, &checker_c) as? EmptyAbleChecker {
            return old
        }
        let new = EmptyAbleChecker(parentView: self, isEmpty: { [weak self] () -> Bool in
            guard let self = self, let dataSource = self.dataSource else { return true }
            let numberOfSections = dataSource.numberOfSections?(in: self) ?? 1
            return !(0 ..< numberOfSections).contains {
                dataSource.collectionView(self, numberOfItemsInSection: $0) > 0
            }
        })
        objc_setAssociatedObject(self, &checker_c, new, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        return new
    }

    @objc private func swizzled_reloadData() {
        swizzled_reloadData()
        emptyAbleChecker.checkIfEmpty(emptyView)
    }

    private static func swizzling() {
        DispatchQueue._once(token: swizzleKey_c) {
            _exchangeSelector(#selector(UICollectionView.reloadData), by: #selector(UICollectionView.swizzled_reloadData))
        }
    }
}


private class EmptyAbleChecker {
    private var layoutBag = DisposeBag()
    private weak var parentView: UIScrollView?
    private weak var target: EmptyAble?
    private var firstLoad: Bool = false
    private let isEmpty: () -> Bool

    required init(parentView: UIScrollView, isEmpty: @escaping (() -> Bool)) {
        self.parentView = parentView
        self.isEmpty = isEmpty
    }

    func checkIfEmptyIfLoaded(_ target: EmptyAble?) {
        if firstLoad {
            checkIfEmpty(target)
        }
    }

    func checkIfEmpty(_ target: EmptyAble?) {
        if !firstLoad {
            firstLoad = true
            return
        }

        if self.target !== target {
            self.target?.removeFromSuperview()
            self.target = target
        }

        guard let emptyView = target else { return }
        var isEmpty = true
        defer {
            emptyView.isHidden = !isEmpty
        }

        // 自定义过滤器
        if let filter = emptyView.customFilter {
            isEmpty = filter()
        } else {
            isEmpty = self.isEmpty()
        }

        if let parentView = parentView, isEmpty {
            if emptyView.superview != parentView {
                emptyView.removeFromSuperview()
                parentView.addSubview(emptyView)

                layoutBag = DisposeBag()
                parentView.rx.frame
                    .distinctUntilChanged()
                    .subscribe(onNext: { [weak self] _ in
                        self?.updateEmptyViewFrame()
                    })
                    .disposed(by: layoutBag)
            } else {
                updateEmptyViewFrame()
            }
        }
    }

    private func updateEmptyViewFrame() {
        guard let parentView = parentView, let emptyView = target else { return }

        var contentInset = parentView.contentInset
        if #available(iOS 11.0, *) {
            contentInset = parentView.adjustedContentInset
        }
        let frame = parentView.bounds.inset(by: contentInset).inset(by: emptyView.edgeInsets)
        
        emptyView.frame = frame
    }
}


