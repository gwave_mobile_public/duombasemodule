//
//  ShakeObservable.swift
//  DuomBase
//
//  Created by kuroky on 2023/6/28.
//

import Foundation
import CoreMotion
import RxCocoa
import RxSwift

public let SHAKE_NOTIFIFY = Notification.Name("GlobalFeedback")

public final class ShakeKit {
    var motionManager: CMMotionManager = CMMotionManager()
    var lastShakeData: Date = Date.distantPast
    
    public static let shared = ShakeKit()
    
    public func startMonitorShake()  {
        guard motionManager.isAccelerometerAvailable else {
            Logger.log(message: "motionManager.isAccelerometerAvailable == false", level: .info)
            return
        }
        
        motionManager.accelerometerUpdateInterval = 0.1
        
        motionManager.startAccelerometerUpdates()
        
        motionManager.startAccelerometerUpdates(to: OperationQueue()) { [weak self] data, error in
            guard let self = self else { return }
            if let data = data {
                let acceleration = data.acceleration
                let accelerameter = sqrt(pow(acceleration.x, 2) + pow(acceleration.y, 2))
                if accelerameter > 3.5 {
                    let currentDate = Date()
                    if currentDate.timeIntervalSince(self.lastShakeData) < 2 {
                        self.lastShakeData = currentDate
                        return
                    }
                    
                    self.lastShakeData = currentDate
                    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\(currentDate)")
                    NotificationCenter.default.post(name: SHAKE_NOTIFIFY, object: nil)
                }
            }
        }
    }
    
    public func stopMonitorShake() {
        self.motionManager.stopAccelerometerUpdates()
    }
    
    
}
