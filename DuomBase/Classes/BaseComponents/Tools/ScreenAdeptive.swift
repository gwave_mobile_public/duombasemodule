//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//


import UIKit

public extension UIViewController {
    // iOS scrollview 及其子类带有导航栏时自动调整内边距问题
    func adjustScrollContentInset(_ scrollView: UIScrollView?) {
        if #available(iOS 11.0, *) {
            scrollView?.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
}

public extension UIScreen {
    // Size
    static var isFullScreen: Bool {
        if #available(iOS 11, *) {
            guard let w = UIApplication.shared.delegate?.window, let unwrapedWindow = w else {
                return false
            }

            if unwrapedWindow.safeAreaInsets.bottom > 0 {
                print(unwrapedWindow.safeAreaInsets)
                return true
            }
        }
        return false
    }
    
    static var duomAspectScale: CGFloat = min(1.5, max(1.f, UIScreen.main.bounds.size.width / 375.f))

    static var screenWidth: CGFloat {
        return UIScreen.main.bounds.size.width
    }

    static var screenHeight: CGFloat {
        return UIScreen.main.bounds.size.height
    }

    static var navigationBarHeight: CGFloat {
        return statusBarHeight + 44
    }

    static var statusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height
    }

    static var bottomBarHeight: CGFloat {
        return isFullScreen ? 83 : 49
    }

    static var topOffset: CGFloat {
        if #available(iOS 11.0, *), let keywindow = UIApplication.shared.keyWindow {
            return keywindow.safeAreaInsets.top
        } else {
            return 0
        }
    }

    static var bottomOffset: CGFloat {
        if #available(iOS 11.0, *), let keywindow = UIApplication.shared.keyWindow {
            return keywindow.safeAreaInsets.bottom
        } else {
            return 0
        }
    }
}
