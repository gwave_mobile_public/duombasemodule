//
//  Camera.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import AVFoundation

class CameraProposer: Proposer {
    var currentStatus: Permission.Status {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch status {
        case .authorized: return .authorized
        case .restricted, .denied: return .denied
        case .notDetermined: return .notDetermined
        default: return .disabled
        }
    }
    
    required init() {}
    
    func proposeToAccess(_ callback: @escaping Permission.Callback) {
        guard let _ = Bundle.main.object(forInfoDictionaryKey: .cameraUsageDescription) else {
            print("WARNING: \(String.cameraUsageDescription) not found in Info.plist")
            return
        }
        
        AVCaptureDevice.requestAccess(for: .video) { _ in
            callback(self.currentStatus)
        }
    }
}
