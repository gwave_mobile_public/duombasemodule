//
//  Photos.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import Photos

class PhotosProposer: Proposer {
    var currentStatus: Permission.Status {
        let status = PHPhotoLibrary.authorizationStatus()
        
        switch status {
        case .authorized: return .authorized
        case .denied, .restricted: return .denied
        case .notDetermined: return .notDetermined
        default: return .disabled
        }
    }
    
    required init() {}
    
    func proposeToAccess(_ callback: @escaping Permission.Callback) {
        guard let _ = Bundle.main.object(forInfoDictionaryKey: .photoLibraryUsageDescription) else {
            print("WARNING: \(String.photoLibraryUsageDescription) not found in Info.plist")
            return
        }
        
        PHPhotoLibrary.requestAuthorization { _ in
            callback(self.currentStatus)
        }
    }
}
