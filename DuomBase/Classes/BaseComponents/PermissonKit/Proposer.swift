//
//  Proposer.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import Foundation

public protocol Proposer: class {
    var currentStatus: Permission.Status { get }

    func proposeToAccess(_ callback: @escaping Permission.Callback)
}
