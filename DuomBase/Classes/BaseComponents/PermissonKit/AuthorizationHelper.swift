//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import AVFoundation
import Foundation
import Photos
import RxSwift
import UIKit

private let appName = (Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String) ?? ""

public enum AuthorizationError: Error, LocalizedError {
    case recordPermissionFailure
    case cameraPermissionFailure
    case albumPermissionFailure
    case locationPermissionFailure
    case notificationPermissionFailure

    public var errorDescription: String? {
        switch self {
        case .recordPermissionFailure:
            return "Setting > Privacy > Microphone，allow \(appName) use"
        case .cameraPermissionFailure:
            return "Setting > Privacy > Camera，allow \(appName) use"
        case .albumPermissionFailure:
            return "Setting > Privacy > Album，allow \(appName) use"
        case .locationPermissionFailure:
            return "Setting > Privacy > Location，allow \(appName) use"
        case .notificationPermissionFailure:
            return "Setting > Notification > \(appName)，allow"
        }
    }
}

public struct CheckAuthorizationOption: OptionSet {
    public let rawValue: Int
    public static let recordPermission = CheckAuthorizationOption(rawValue: 1 << 0)
    public static let cameraPermission = CheckAuthorizationOption(rawValue: 1 << 1)
    public static let albumPermission = CheckAuthorizationOption(rawValue: 1 << 2)

    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
}

public class AuthorizationHelper {
    public class func authorizationRecordPermission() -> Completable {
        return Completable.create { completable -> Disposable in
            switch AVAudioSession.sharedInstance().recordPermission {
            case .denied:
                completable(.error(AuthorizationError.recordPermissionFailure))
            case .granted:
                completable(.completed)
            case .undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission { granted in
                    DispatchQueue.main.async {
                        if granted {
                            completable(.completed)
                        } else {
                            completable(.error(AuthorizationError.recordPermissionFailure))
                        }
                    }
                }
            @unknown default:
                completable(.error(AuthorizationError.recordPermissionFailure))
            }
            return Disposables.create()
        }
    }

    public class func authorizationCameraPermission() -> Completable {
        return Completable.create { completable -> Disposable in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .denied, .restricted:
                completable(.error(AuthorizationError.cameraPermissionFailure))
            case .authorized:
                completable(.completed)
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    DispatchQueue.main.async {
                        if granted {
                            completable(.completed)
                        } else {
                            completable(.error(AuthorizationError.cameraPermissionFailure))
                        }
                    }
                }
            @unknown default:
                completable(.error(AuthorizationError.cameraPermissionFailure))
            }
            return Disposables.create()
        }
    }

    public class func authorizationAlbumPermission() -> Completable {
        return Completable.create { completable -> Disposable in
            switch PHPhotoLibrary.authorizationStatus() {
            case .denied, .restricted:
                completable(.error(AuthorizationError.albumPermissionFailure))
            case .authorized, .limited:
                completable(.completed)
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization { status in
                    DispatchQueue.main.async {
                        if status == .authorized {
                            completable(.completed)

                        } else {
                            completable(.error(AuthorizationError.albumPermissionFailure))
                        }
                    }
                }
            @unknown default:
                completable(.error(AuthorizationError.albumPermissionFailure))
            }
            return Disposables.create()
        }
    }

    public class func authorizationLocationPermission() -> Completable {
        return Completable.create { (completable) -> Disposable in
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                AuthorizationShared.shared.locationObservers.append(completable)
                AuthorizationShared.shared.locationManager.requestWhenInUseAuthorization()
            case .denied, .restricted:
                completable(.error(AuthorizationError.locationPermissionFailure))
            default:
                completable(.completed)
            }
            return Disposables.create()
        }
    }

    public class func authorizationNotificationPermission() -> Completable {
        return Completable.create { completable -> Disposable in
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().getNotificationSettings { setting in
                    if setting.authorizationStatus != .notDetermined {
                        completable(.completed)
                    }
                }
                return Disposables.create()
            } else {
                if (UIApplication.shared.currentUserNotificationSettings?.types ?? []) != [] {
                    completable(.completed)
                    return Disposables.create()
                }
            }
            
            if #available(iOS 10.0, *) {
//                let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
//                UIApplication.shared.registerUserNotificationSettings(settings)
                UIApplication.shared.registerForRemoteNotifications()
                UNUserNotificationCenter.current()
                    .requestAuthorization(options: [.badge, .sound, .alert],
                                          completionHandler: { granted, _ in
                                              DispatchQueue.main.async {
                                                  if granted {
                                                      completable(.completed)
                                                  } else {
                                                      completable(.error(AuthorizationError.notificationPermissionFailure))
                                                  }
                                              }
                })
                return Disposables.create()

            } else {
                let disposable = NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification)
                    .take(1)
                    .delay(.milliseconds(200), scheduler: MainScheduler.instance)
                    .timeout(.seconds(60), scheduler: MainScheduler.instance)
                    .subscribe(onNext: { _ in
                        if (UIApplication.shared.currentUserNotificationSettings?.types ?? []) != [] {
                            completable(.completed)
                        } else {
                            completable(.error(AuthorizationError.notificationPermissionFailure))
                        }
                    }, onError: { _ in
                        completable(.error(AuthorizationError.notificationPermissionFailure))
                    })

                let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
                UIApplication.shared.registerForRemoteNotifications()
                return disposable
            }
        }
    }
}

public extension AuthorizationHelper {
    class func checkAuthorizationWithOutAlert(option: CheckAuthorizationOption) -> Completable {
        var observables = [RxSwift.Observable<Never>]()
        if option.contains(.cameraPermission) {
            observables.append(authorizationCameraPermission().asObservable())
        }
        if option.contains(.recordPermission) {
            observables.append(authorizationRecordPermission().asObservable())
        }
        if option.contains(.albumPermission) {
            observables.append(authorizationAlbumPermission().asObservable())
        }
        
        return Completable.create { completable -> Disposable in
            Observable.zip(observables)
                .subscribe(onError: { error in
                    completable(.error(error))
                }, onCompleted: {
                    completable(.completed)
                })
        }
    }
    
    class func checkAuthorizationWithAlert(option: CheckAuthorizationOption,
                                           from: UIViewController) -> Completable {
        var observables = [RxSwift.Observable<Never>]()
        if option.contains(.cameraPermission) {
            observables.append(authorizationCameraPermission().asObservable())
        }
        if option.contains(.recordPermission) {
            observables.append(authorizationRecordPermission().asObservable())
        }
        if option.contains(.albumPermission) {
            observables.append(authorizationAlbumPermission().asObservable())
        }

        return Completable.create { completable -> Disposable in
            Observable.zip(observables)
                .subscribe(onError: { error in
                    if !error.localizedDescription.isEmpty, let error = error as? AuthorizationError {
                        var title: String?
                        switch error {
                        case .recordPermissionFailure:
                            title = "Record Permission Failure"
                        case .cameraPermissionFailure:
                            title = "Camera Permission Failure"
                        case .albumPermissionFailure:
                            title = "Album Permission Failure"
                        default:
                            completable(.error(error))
                        }

                        if let title = title {
                            let alert = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { _ in
                                completable(.error(error))
                            }))
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                completable(.error(error))
                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                            }))
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                from.showDetailViewController(alert, sender: nil)
                            }
                        }
                    } else {
                        completable(.error(error))
                    }
                }, onCompleted: {
                    completable(.completed)
                })
        }
    }
}

private class AuthorizationShared: NSObject, CLLocationManagerDelegate {
    static let shared = AuthorizationShared()
    var locationObservers = [PrimitiveSequenceType.CompletableObserver]()

    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        return manager
    }()

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationObservers.forEach {
                $0(.completed)
            }
            locationObservers.removeAll()
        case .notDetermined:
            break
        case .denied, .restricted:
            locationObservers.forEach {
                $0(.error(AuthorizationError.locationPermissionFailure))
            }
            locationObservers.removeAll()
        @unknown default:
            break
        }
    }
}
