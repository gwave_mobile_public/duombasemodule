//
//  HUD.swift
//  DuomBase
//
//  Created by kuroky on 2023/5/25.
//

import Foundation
import MBProgressHUD

// 定制hud
private var HUDKeepStayKey: Void?

private extension MBProgressHUD {
    var isKeepStay: Bool {
        get {
            return (objc_getAssociatedObject(self, &HUDKeepStayKey) as? Bool) ?? false
        }
        set {
            objc_setAssociatedObject(self, &HUDKeepStayKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

public class HUD {
    @discardableResult
    public static func showImage(_ image: UIImage? = UIImage(named: "loading_hud"), text: String? = nil, autoDismissDelay: Double = 3) -> MBProgressHUD {
        let imageView = UIImageView(image: image)
        let hud = showNewHUD(imageView, insets: UIEdgeInsets(top: 0, left: 0, bottom: 6, right: 0))
        hud.label.text = text
        if autoDismissDelay > 0 {
            hud.hide(animated: true, afterDelay: autoDismissDelay)
        } else {
            hud.isKeepStay = true
        }

        return hud
    }
    
    public static func dismiss(_ animated: Bool = true, immediately: Bool = false) {
        visibleHUDs().forEach {
            if $0.isKeepStay || immediately {
                $0.removeFromSuperViewOnHide = true
                $0.hide(animated: animated)
            }
        }
    }

    public static func visibleHUDs() -> [MBProgressHUD] {
        return UIApplication.shared.windows.flatMap { $0.subviews.compactMap { $0 as? MBProgressHUD } }
    }
    
    private static func showNewHUD(_ customView: UIView, insets: UIEdgeInsets = .zero) -> MBProgressHUD {
        let contentView = UIView()
        contentView.addSubview(customView)
        customView.snp.makeConstraints { make in
            make.edges.equalTo(insets)
        }

        let hud = MBProgressHUD.showAdded(to: frontWindow, animated: true)
        hud.bezelView.subviews.forEach {
            if let view = $0 as? UIVisualEffectView {
                view.removeFromSuperview()
            }
        }
        hud.isUserInteractionEnabled = false
        hud.isSquare = false
        hud.bezelView.layer.cornerRadius = 8
        hud.bezelView.color = UIColor.black.withAlphaComponent(0.33)
        hud.bezelView.blurEffectStyle = .dark
        hud.customView = contentView
        hud.mode = .customView
        hud.label.numberOfLines = 0
        hud.label.textColor = .white
        hud.detailsLabel.textColor = .white
        hud.detailsLabel.numberOfLines = 0

        hud.bezelView.bringSubviewToFront(hud.label)
        hud.bezelView.bringSubviewToFront(hud.detailsLabel)
        hud.bezelView.bringSubviewToFront(hud.button)

        return hud
    }

    private static var frontWindow: UIWindow {
        let window = UIApplication.shared.windows.last(where: {
            $0.screen == UIScreen.main &&
                !$0.isHidden && $0.alpha > 0 &&
                $0.windowLevel == UIWindow.Level.normal
        })
        return window ?? (UIApplication.shared.keyWindow ?? UIWindow())
    }
}
