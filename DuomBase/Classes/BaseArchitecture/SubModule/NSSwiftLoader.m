//
//  NSSwiftLoader.m
//  VCSubModule
//
//  Created by kuroky on 2022/11/2.
//

#import "NSSwiftLoader.h"
#import <objc/runtime.h>
#import <dlfcn.h>
#import <mach-o/ldsyms.h>

@implementation NSSwiftLoader

+(void)load {
    int numClasses = 0, newNumClasses = objc_getClassList(NULL, 0);
    Class *classes = NULL;
    
    while (numClasses < newNumClasses) {
        numClasses = newNumClasses;
        classes = (Class *)realloc(classes, sizeof(Class) * numClasses);
        newNumClasses = objc_getClassList(classes, numClasses);
        
        for (int i = 0; i < numClasses; i++) {
            Class class = classes[i];
            if (class_conformsToProtocol(class, @protocol(NSSwiftLoadProtocol))) {
                [(id<NSSwiftLoadProtocol>)class swiftLoad];
            }
        }
    }
    free(classes);
}

@end
