//
//  SubModuleService.swift
//  Differentiator
//
//  Created by kuroky on 2022/11/16.
//

import Foundation

private final class WeakRef {
    private(set) weak var object: AnyObject?
    init(_ object: AnyObject) {
        self.object = object
    }
}

public final class SubModuleService {
    private var weakRepositoryRef: WeakRef?
    private var strongRePository: AnyObject?
    
    private var subModules: [WeakRef] = []
    public var repository: AnyObject? {
        return strongRePository ?? weakRepositoryRef?.object
    }
    
    public func inject(_ repository: AnyObject?) {
        strongRePository = repository
        weakResolve(repository)
    }
    
    public func appendSubModule(_ module: SubModuleService) {
        if !subModules.contains(where: { $0 === module }) {
            let ref = WeakRef(module)
            subModules.append(ref)
            module.weakResolve(repository)
        }
    }
    
    private func weakResolve(_ repository: AnyObject?)  {
        if let repository = repository {
            weakRepositoryRef = WeakRef(repository)
        } else {
            weakRepositoryRef = nil
        }
        
        subModules = subModules.filter { $0.object != nil }
        let modules = subModules.compactMap { $0.object as? SubModuleService }
        modules.forEach {
            $0.weakResolve(repository)
        }
    }
}
