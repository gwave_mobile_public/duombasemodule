//
//  ViewControllerSubModuleLoader.swift
//  DuomBase
//
//  Created by kuroky on 2022/11/16.
//

import Foundation
import UIKit

private class ViewControllerSubModuleLoader: NSSwiftLoadProtocol {
    static func swiftLoad() {
        ViewControllerSubmoduleInjection.setPerfromWithDestination { from, to in
            self.onMainThread {
                if let fromService = findViewControllerSubModuleTarget(from)?.last(where: { $0.getSubModuleService() != nil })?.getSubModuleService(),
                   let tos = findViewControllerSubModuleTarget(to) {
                    tos.forEach { to in
                        fromService.appendSubModule(to.subModuleService)
                    }
                }
            }
        }
    }
    
    static func onMainThread(_ task: @escaping () -> Void) {
        if Thread.isMainThread {
            task()
        } else {
            DispatchQueue.main.sync {
                task()
            }
        }
    }
    
    private static func findViewControllerSubModuleTarget(_ arg: Any) -> [ViewControllerSubModule]?  {
        if let nav = arg as? UINavigationController {
            return [nav] + nav.viewControllers
        } else if let arg = arg as? UIViewController {
            return [arg]
        }
        return nil
    }
}

public protocol ViewControllerSubModule: AnyObject {
    var subModuleService: SubModuleService { get }
}

extension UIViewController: ViewControllerSubModule {}

private var SubModuleServiceKey: Void?
public extension ViewControllerSubModule {
    var subModuleService: SubModuleService {
        var instance = getSubModuleService()
        if instance == nil {
            instance = SubModuleService()
            objc_setAssociatedObject(self, &SubModuleServiceKey, instance, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        return instance!
    }
    
    fileprivate func getSubModuleService() -> SubModuleService? {
        return objc_getAssociatedObject(self, &SubModuleServiceKey) as? SubModuleService
    }
}

