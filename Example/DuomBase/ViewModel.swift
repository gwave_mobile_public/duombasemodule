//
//  ViewModel.swift
//  DuomBase_Example
//
//  Created by kuroky on 2022/9/2.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import DuomBase
import RxCocoa
import RxSwift

class TViewModel: ViewModel {
    
    var refresher: Refresher?
    
    var curText: String = "123"
    
    enum Action: ResponderChainEvent {
        case testA
        case testB
    }
    
    class State: Output {
        var testA_PublishSubject = PublishSubject<Int>()
        var testB_PublishSubject = PublishSubject<Int>()
        required init() {}
    }
    
    func mutate(action: Action, state: State) {
        switch action {
        case .testA:
            state.testA_PublishSubject.onNext(11111)
        case .testB:
            state.testA_PublishSubject.onNext(22222)
        }
    }
    
    
    
}
