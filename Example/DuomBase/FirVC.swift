//
//  FirVC.swift
//  DuomBase_Example
//
//  Created by kuroky on 2022/11/16.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import DuomBase
import MessageUI

class FirVC: PresentationViewController, ViewType {
    func bind(viewModel: TViewModel) {
        viewModel.state.testB_PublishSubject
            .subscribe(onNext: { num in
                print("firstVC:::::::::\(num)")
            }).disposed(by: disposeBag)
        
        viewModel.state.testA_PublishSubject
            .subscribe(onNext: { num in
                print("firstVC::::::::: \(num)")
            }).disposed(by: disposeBag)
    }
    
    typealias TargetViewModel = TViewModel
    
    let bview = UIView()
    
    let doubleBtn: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        
//        view.addSubview(bview)
//        bview.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//            make.size.equalTo(CGSize(width: UIScreen.screenWidth, height: 500))
//        }
        
        view.addSubview(doubleBtn)
        doubleBtn.backgroundColor = .yellow
        doubleBtn.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 300, height: 200))
        }
        
        doubleBtn.addTarget(self, action: #selector(doubleACtion(_:)), for: .touchUpInside)
    }
    
    @objc func doubleACtion(_ sender: UIButton) {
        let alert = DuomAlertCardViewController(title: "Discard",
                                                content: "Your photo has not been saved yet. Are you sure you want to discard?",
                                                insideItem: DuomAlertCardItemConfig(title: "Cancel", dismissOnTap: true, font: .appFont(ofSize: 20, fontType: .TT_Regular), titleColor: UIColor(6, 5, 6), backgroundColor: .clear, highlightedColor: .clear),
                                                outsideItem: DuomAlertCardItemConfig.init(title: "Confirm", backgroundColor: UIColor(83, 89, 214), highlightedColor: UIColor(110, 116, 240)))
     
        UIApplication.topViewController()?.present(alert, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       
        let titleStr = "Discard edits?"


        let desStr = "Making screenshots"

//        let desStr = "Your photo has not been saved yet. Are you sure you want to discard?Your photo has not been saved yet. Are you sure you want to discard?Your photo has not been saved yet. Are you sure you want to discard?"

        lazy var screenCaptureAlert: UIAlertController = UIAlertController(title: titleStr, message: desStr, preferredStyle: .alert)

      //  lazy var screenShotAlert: UIAlertController = UIAlertController(title: titleStr, message: desStr, preferredStyle: .alert).then {
      //    let alertAction = UIAlertAction(title: "I agree", style: UIAlertAction.Style.default)
      //    $0.addAction(alertAction)
      //  }

        lazy var screenShotAlert: DuomAlertCardViewController = DuomAlertCardViewController(title: titleStr,
                                                                                                content: desStr,
                                                                                            insideItem: nil,
                                                                                                outsideItem: DuomAlertCardItemConfig(title: "I agree", backgroundColor: UIColor(83, 89, 214), highlightedColor: UIColor(110, 116, 240)))

        UIApplication.topViewController()?.present(screenShotAlert, animated: true)
    }
    
    
}





