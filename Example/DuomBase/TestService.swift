//
//  TestService.swift
//  DuomBase_Example
//
//  Created by kuroky on 2022/11/16.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import DuomBase

//private var lastSubModuleRec = NSMapTable<NSString, AnyObject>.strongToWeakObjects()
//private var lastViewModel: ViewModel?
//
//private extension ViewControllerSubModule {
//    func findRepository<T>(_ type: T.Type) -> T? {
//        let recKey = String(describing: type) as NSString
//        if let repository = subModuleService.repository as? T {
//            lastSubModuleRec.setObject(repository as AnyObject, forKey: recKey)
//            return repository
//        }
//        return lastSubModuleRec.object(forKey: recKey) as? T
//    }
//}
//
//public protocol TestSubModule: ViewControllerSubModule {}
//
//extension TestSubModule where Self: UIViewController {
//    var viewModel: ViewModel {
//        if let repository = findRepository(ViewModel.self) {
//            lastViewModel = repository
//            return repository
//        }
//        
//        if let lastVM = lastViewModel {
//            return lastVM
//        }
//        
//        fatalError("not find repository in ViewController Chain! \(self)")
//    }
//}
