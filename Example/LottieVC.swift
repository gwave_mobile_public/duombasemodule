//
//  LottieVC.swift
//  DuomBase_Example
//
//  Created by kuroky on 2023/6/27.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import DuomBase
import Lottie

class LottieVC: BaseViewController {
    
    let startBtn: UIButton = UIButton().then {
        $0.setTitle("start", for: .normal)
        $0.backgroundColor = .red
    }
    
    let stopBtn: UIButton = UIButton().then {
        $0.setTitle("stop", for: .normal)
        $0.backgroundColor = .blue
    }
    
    let pauseBtn: UIButton = UIButton().then {
        $0.setTitle("pause", for: .normal)
        $0.backgroundColor = .blue
    }
    
    let animateView: LottieAnimationView = LottieAnimationView(name: "noget_gems").then {
        $0.contentMode = .scaleAspectFill
        $0.loopMode = .loop
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        view.addSubview(stopBtn)
        view.addSubview(startBtn)
        view.addSubview(pauseBtn)
        view.addSubview(animateView)
        
        animateView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.equalTo(20)
            make.trailing.equalTo(-20)
            make.height.equalTo(300)
        }
        
        startBtn.snp.makeConstraints { make in
            make.left.equalTo(animateView.snp.left)
            make.top.equalTo(animateView.snp.bottom).offset(50)
            make.size.equalTo(CGSize(width: 100, height: 50))
        }
        
        stopBtn.snp.makeConstraints { make in
            make.right.equalTo(animateView.snp.right)
            make.top.equalTo(animateView.snp.bottom).offset(50)
            make.size.equalTo(CGSize(width: 100, height: 50))
        }
        
        pauseBtn.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(animateView.snp.bottom).offset(50)
            make.size.equalTo(CGSize(width: 100, height: 50))
        }
        
        stopBtn.addTarget(self, action: #selector(stopAction(_:)), for: .touchUpInside)
        startBtn.addTarget(self, action: #selector(startAction(_:)), for: .touchUpInside)
        pauseBtn.addTarget(self, action: #selector(pauseAction(_:)), for: .touchUpInside)
    }
    
    
    @objc func startAction(_ sender: UIButton) {
        animateView.play()
    }
    
    @objc func stopAction(_ sender: UIButton) {
        animateView.stop()
    }
    
    @objc func pauseAction(_ sender: UIButton) {
        animateView.pause()
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let secVC = SecVC()
//        self.present(secVC, animated: true)
//        return
//    }
}
