#
# Be sure to run `pod lib lint DuomBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'DuomBase'
    s.version          = '0.2.5'
    s.summary          = 'Duom 基础组件&基础架构～ 精简版'
    
    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!
    
    s.description      = <<-DESC
    TODO: Add long description of the pod here.
    DESC
    
    s.homepage         = 'https://gitlab.com/gwave_mobile_public/duombasemodule'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'HeKai_Duom' => 'kai.he@duom.com' }
    s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/duombasemodule.git', :tag => s.version.to_s }
    # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
    
    s.ios.deployment_target = '12.0'
    s.swift_version    = '5.0'
    
    s.resources = 'DuomBase/Assets/*', 'DuomBase/Others/*'
    
    s.subspec 'Account' do |spec|
        spec.source_files = 'DuomBase/Classes/Account/**/*'
    end
    
    s.subspec 'Language' do |spec|
        spec.source_files = 'DuomBase/Classes/Language/**/*'
    end
    
    s.subspec 'BaseArchitecture' do |spec|
        spec.subspec 'Action&State' do |action|
            action.source_files = 'DuomBase/Classes/BaseArchitecture/Action&State/**/*'
        end
        spec.subspec 'SubModule' do |subModule|
            subModule.source_files = 'DuomBase/Classes/BaseArchitecture/SubModule/**/*'
        end
    end
    
    s.subspec 'BaseComponents' do |spec|
        spec.subspec 'BaseViews' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/BaseViews/**/*'
        end
        
        spec.subspec 'DuomAlertViewController' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/DuomAlertViewController/**/*'
        end
        
        spec.subspec 'DuomViews' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/DuomViews/**/*'
        end
        
        spec.subspec 'PermissonKit' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/PermissonKit/**/*'
        end
        
        spec.subspec 'PresentionViewController' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/PresentionViewController/**/*'
        end
        
        spec.subspec 'Refresher' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/Refresher/**/*'
        end
        
        spec.subspec 'TextAttributes' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/TextAttributes/**/*'
        end
        
        spec.subspec 'Toaster' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/Toaster/**/*'
        end
        
        spec.subspec 'Tools' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/Tools/**/*'
        end
        
        spec.subspec 'UIFont+Extension' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/UIFont+Extension/**/*'
        end
        
        spec.subspec 'DuomPagingView' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/DuomPagingView/**/*'
        end
        
        spec.subspec 'DuomSegmentView' do |ss|
            ss.source_files = 'DuomBase/Classes/BaseComponents/DuomSegmentView/**/*'
        end
        
    end
    
    s.dependency 'RxSwift'
    s.dependency 'RxCocoa'
    s.dependency 'RxRelay'
    s.dependency 'RxOptional'
    s.dependency 'NSObject+Rx'
    s.dependency 'RxViewController'
#    s.dependency 'RxDataSources'
#    s.dependency 'RxKeyboard'
#    s.dependency 'RxGesture'
    s.dependency 'Then'
    s.dependency 'MBProgressHUD'
    s.dependency 'MJRefresh'
    s.dependency 'SnapKit'
    s.dependency 'SwiftyImage'
    s.dependency 'Kingfisher', '~> 7.0'
    
end
